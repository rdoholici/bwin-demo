Feature: Log in the application and place bet

  Scenario: Log in application

    Given the application main page loads and the logo is visible
    When I input the credentials: prodUserRo1 and 123456asd
      And the button with text LOGARE is pressed
    Then the user is logged in

    Scenario: Place bet and log out

      Given I select the sport category: Fotbal
        And I select the first match
      When I select a random bet
      Then the bet appears on the betslip

      Given the bet appears on the betslip
        And the amount is changed to 5.00
      When the button with text Plasaţi pariul is pressed
      Then the text Pariul dvs. simplu a fost acceptat. Mult succes! is displayed
        And the balance is adjusted
        And the user logs out
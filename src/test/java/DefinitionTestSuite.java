import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		plugin = {"pretty"},
		glue = {"stepdefs"},
		features = "src/test/resources/features/Test.feature")
public class DefinitionTestSuite {}

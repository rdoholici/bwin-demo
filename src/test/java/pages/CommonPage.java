package pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import tools.WebUtils;

import java.util.List;

@DefaultUrl("https://sports.sportingbet.ro")
public class CommonPage extends PageObject {

    private static final Logger APP_LOGGER = Logger.getLogger(CommonPage.class.getName());

    private final String pageLogoXpath = "//div[@class='header-logo']";
    private final String elementWithTextXpathFormat = "//%s[contains(text(),'%s')]";
    private final String userInputXpath = "//input[@id='username']";
    private final String pwInputXpath = "//input[@id='password']";
    private final String sportCategotyXpathFormat = "//span[text()='%s'][@class='sports-links__name']";
    private final String selectedCategoryXpathFormat = "//h1[text()='Pariuri %s']";
    private final String matchRowsXpath = "//tr[@class='livewidget__event-row']//a[@class='livewidget__event-name']";
    private final String eventLinkXpath = "//a[@class='activeItem']//span[text()='Eveniment']";
    private final String betsXpath = "//div[@class='option-button rounded']";
    private final String betslipBetsXpath = "//li[contains(@class,'betslip-options__option')]//span[@class='option-name']";
    private final String betStakeXpath = "//input[@id='betslip-stake']";
    private final String currentFundsXpath = "//span[@id='user-state-account-balance']";
    private final String logOutXpath = "//button[contains(text(),'Deconectare')]";

    private String betName;
    private String currentFunds;
    private String betAmount;

    public boolean verifyPageIsLoaded() {
        return getDriver().findElements(By.xpath(pageLogoXpath)).size() > 0;
    }

    public void pressElementWithVisibleTest(String elementType, String visibleText) {
        getDriver().findElement(By.xpath(String.format(elementWithTextXpathFormat,elementType,visibleText))).click();
    }

    public void inputUser(String user) {
        WebElement input = getDriver().findElement(By.xpath(userInputXpath));
        input.clear();
        input.sendKeys(user);
    }

    public void inputPassword(String password) {
        WebElement input = getDriver().findElement(By.xpath(pwInputXpath));
        input.clear();
        input.sendKeys(password);
    }

    public boolean verifyLogInFieldsNotEmpty() {
        boolean toReturn;
        toReturn = getDriver().findElement(By.xpath(userInputXpath)).getAttribute("value").isEmpty() &&
                getDriver().findElement(By.xpath(pwInputXpath)).getAttribute("value").isEmpty();
        return !toReturn;
    }

    public boolean verifyElementWithTextIsVisible(String elementType, String visibleText) {
        return getDriver().findElement(By.xpath(String.format(elementWithTextXpathFormat,elementType,visibleText))).isDisplayed();
    }

    public boolean verifyUserIsLoggedIn() {
        return getDriver().findElements(By.xpath("//a[contains(text(),'Depune')]")).size() > 0;
    }

    public void selectSportCategory(String category) {
        getDriver().findElement(By.xpath(String.format(sportCategotyXpathFormat,category))).click();
    }

    public boolean verifyCategorySelected(String category) {
        return getDriver().findElements(By.xpath(String.format(selectedCategoryXpathFormat,category))).size() > 0;
    }

    public void selectFirstMatch() {
        List<WebElement> matchRowElements = getDriver().findElements(By.xpath(matchRowsXpath));
        matchRowElements.get(0).click();
    }

    public boolean verifyEventPageLoaded() {
        return getDriver().findElements(By.xpath(eventLinkXpath)).size() > 0;
    }

    public boolean selectRandomBet() {
        boolean toReturn = false;
        List<WebElement> matchRowElements = getDriver().findElements(By.xpath(betsXpath));
        int random = WebUtils.generateRandomNumber(0,matchRowElements.size()-1);
        toReturn = matchRowElements.get(random).isDisplayed() && matchRowElements.get(random).isEnabled();
        betName = matchRowElements.get(random).findElement(By.xpath("div[@class='name']/div")).getText();
        WebUtils.scrollToElement(matchRowElements.get(random),getDriver());
        matchRowElements.get(random).click();
        return toReturn;
    }

    public boolean verifyBetslip() throws InterruptedException {
        List<WebElement> betslipBets = getDriver().findElements(By.xpath(betslipBetsXpath));
        int counter = 0;
        while (betslipBets.size()==0 && counter < 5000) {
            counter = counter + 250;
            Thread.sleep(250);
            betslipBets = getDriver().findElements(By.xpath(betslipBetsXpath));
        }
        for (WebElement bet: betslipBets) {
            WebUtils.scrollToElement(bet,getDriver());
            if (bet.getText().trim().equals(betName.trim()))
                return true;
        }
        return  false;
    }

    public boolean verifyTextIsDisplayed(String text) throws InterruptedException {
        List<WebElement> elementToVerify = getDriver().findElements(By.xpath(String.format("//*[contains(text(),'%s')]",text)));
        int counter = 0;
        while (elementToVerify.size()==0 && counter < 5000) {
            counter = counter + 250;
            Thread.sleep(250);
            elementToVerify = getDriver().findElements(By.xpath(String.format("//*[contains(text(),'%s')]",text)));
        }
        return elementToVerify.size() > 0;
    }

    public void setBetAmount(String amount) {
        WebElement stakeInput = getDriver().findElement(By.xpath(betStakeXpath));
        stakeInput.clear();
        stakeInput.sendKeys(amount);
        currentFunds = getDriver().findElement(By.xpath(currentFundsXpath)).getText();
        betAmount = amount;
    }

    public boolean verifyBetAmount(String amount) {
        WebElement stakeInput = getDriver().findElement(By.xpath(betStakeXpath));
        return stakeInput.getAttribute("value").equals(amount);
    }

    public boolean verifyCurrentFunds() {
        String currentBalance = getDriver().findElement(By.xpath(currentFundsXpath)).getText().trim().replace("RON","").replace(",",".");
        float postBetFunds = Float.parseFloat(currentBalance.trim().replace("RON","").replace(",","."));
        float preBetFunds = Float.parseFloat(currentFunds.trim().replace("RON","").replace(",","."));
        float amountUsed = Float.parseFloat(betAmount.trim().replace("RON","").replace(",","."));
        APP_LOGGER.info("Current funds: " + postBetFunds);
        APP_LOGGER.info("Amount used: " + amountUsed);
        APP_LOGGER.info("Before betting funds: " + preBetFunds);
        System.out.println(preBetFunds - amountUsed == postBetFunds);
        return preBetFunds - amountUsed == postBetFunds;
    }

    public void logOut() {
        getDriver().findElement(By.xpath(logOutXpath)).click();
    }

}

package tools;

import net.serenitybdd.core.annotations.findby.By;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.ui.*;
import org.yecht.Data;


import java.io.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static  net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

/**
 * Created by Turtapod on 4/18/2018.
 */
public class WebUtils {

	private static final org.apache.log4j.Logger APP_LOGGER = org.apache.log4j.Logger.getLogger(WebUtils.class.getName());

	public static void scrollToElement(WebElement element, WebDriver driver) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
	}

	public static int generateRandomNumber(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
}

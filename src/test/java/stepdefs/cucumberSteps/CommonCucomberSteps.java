package stepdefs.cucumberSteps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.log4j.Logger;
import stepdefs.serenitySteps.CommonSerenitySteps;

public class CommonCucomberSteps {

    private static final Logger APP_LOGGER = Logger.getLogger(CommonCucomberSteps.class.getName());

    @Steps
    private CommonSerenitySteps commonSerenitySteps;

    @Given("^the application main page loads and the logo is visible$")
    public void verify_application_is_loadded() {
        APP_LOGGER.info("Verifying the application main page loads.");
        commonSerenitySteps.verifyApplicationIsLoaded();
    }

    @When("^I input the credentials: (.*) and (.*)$")
    public void input_credentials(String user, String password) {
        APP_LOGGER.info("Using credentiasls: " + user + " and " + password);
        commonSerenitySteps.inputCredentials(user,password);
    }

    @And("^the (.*) with text (.*) is pressed$")
    public void press_element_with_visible_text(String elementType, String visibleText) {
        APP_LOGGER.info("Pressing element: " + elementType + " with text: " + visibleText);
        commonSerenitySteps.pressElementWithVisibleText(elementType,visibleText);
    }

    @Then("^the user is logged in$")
    public void verify_user_is_logged_in() {
        APP_LOGGER.info("Verifying user is logged in.");
        commonSerenitySteps.verfyUserIsLoggedIn();
    }

    @Given("^I select the sport category: (.*)$")
    public void select_sport_category(String category) {
        APP_LOGGER.info("Selecting category: " + category);
        commonSerenitySteps.selectCategory(category);
    }

    @And("^I select the first match$")
    public void select_first_match() {
        APP_LOGGER.info("Selecting random match.");
        commonSerenitySteps.selectFirstMatch();
    }

    @When("^I select a random bet$")
    public void select_bet() {
        APP_LOGGER.info("Selecting a random bet.");
        commonSerenitySteps.selectRandomBet();
    }

    @Then("^the bet appears on the betslip$")
    public void verify_betslip_not_empty() {
        APP_LOGGER.info("Verifying betslip contains bet.");
        commonSerenitySteps.verifyBetSlip();
    }

    @Then("^the text (.*) is displayed$")
    public void verify_text_is_displayed(String text) {
        APP_LOGGER.info("Verifying text: " + text + " is present in page.");
        commonSerenitySteps.verifyTextIsDisplayed(text);
    }

    @And("^the amount is changed to (.*)$")
    public void change_bet_amount(String amount) {
        APP_LOGGER.info("Setting bet amount to: " + amount);
        commonSerenitySteps.setBetAmount(amount);
    }

    @And("^the balance is adjusted$")
    public void verify_current_funds() {
        APP_LOGGER.info("Verifying current funds are adjusted correctly.");
        commonSerenitySteps.verifyCurrentFunds();
    }

    @And("^the user logs out$")
    public void log_out(){
        APP_LOGGER.info("Logging out.");
        commonSerenitySteps.logOut();
    }
}

package stepdefs.serenitySteps;

import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;
import org.junit.Assert;
import pages.CommonPage;

public class CommonSerenitySteps {

    private static final Logger APP_LOGGER = Logger.getLogger(CommonSerenitySteps.class.getName());
    private CommonPage commonPage;

    @Step
    public void verifyApplicationIsLoaded() {
        commonPage.open();
        Assert.assertTrue("The page logo is not displayed",
            commonPage.verifyPageIsLoaded());
        APP_LOGGER.info("The main page loaded successfully.");
    }

    @Step
    public void inputCredentials(String user, String password) {
        commonPage.pressElementWithVisibleTest("a","Autentificare");
        commonPage.inputUser(user);
        commonPage.inputPassword(password);
        Assert.assertTrue("The input and password fields are empty",
                commonPage.verifyLogInFieldsNotEmpty());
    }

    @Step
    public void pressElementWithVisibleText(String elementType, String visibleText) {
        Assert.assertTrue("The element: " + elementType + " with text: " + visibleText + " is not visible.",
                commonPage.verifyElementWithTextIsVisible(elementType,visibleText));
        commonPage.pressElementWithVisibleTest(elementType,visibleText);
        APP_LOGGER.info("Element clicked.");
    }

    @Step
    public void verfyUserIsLoggedIn() {
        Assert.assertTrue("The user is not properly logged in.",
                commonPage.verifyUserIsLoggedIn());
    }

    @Step
    public void selectCategory(String category) {
        commonPage.selectSportCategory(category);
        Assert.assertTrue("Category was not selected.",
                commonPage.verifyCategorySelected(category));
    }

    @Step
    public void selectFirstMatch() {
        commonPage.selectFirstMatch();
        Assert.assertTrue("The event page has not loaded.",
                commonPage.verifyEventPageLoaded());
    }

    @Step
    public void selectRandomBet() {
        Assert.assertTrue("The bet button could not be clicked.",
                commonPage.selectRandomBet());
    }

    @Step
    public void verifyBetSlip() {
        try {
            Assert.assertTrue("The bet is not present in the betslip.",
                    commonPage.verifyBetslip());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void verifyTextIsDisplayed(String text) {
        try {
            Assert.assertTrue("The text: " + text + " is not displayed in page.",
                    commonPage.verifyTextIsDisplayed(text));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step
    public void setBetAmount(String amount) {
        commonPage.setBetAmount(amount);
        Assert.assertTrue("The bet amount is not set correctly.",
                commonPage.verifyBetAmount(amount));
    }

    public void verifyCurrentFunds() {
        Assert.assertTrue("The current funds are not correclty calculated.",
                commonPage.verifyCurrentFunds());
    }

    @Step
    public void logOut() {
        commonPage.logOut();
        Assert.assertTrue("The user is not logged out.",
            !commonPage.verifyUserIsLoggedIn());
    }
}
